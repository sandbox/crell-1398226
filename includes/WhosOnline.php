<?php

namespace Drupal\Module\cats;

use Drupal\Core\Controller\BlockController;

use Drupal\Core\Request;

// This is CMI stuff.
use Drupal\Core\Configuration\Config;

/**
 * @file
 *
 * Description goes here.
 */

/**
 * Description of NodeViewController
 */
class WhosOnline extends BlockController {

  // Note the absence of access checks.  Those really ought to be separate
  // from the block class itself.  We're not sure quite yet how that will work.

  // Separate method, as it should be.
  public function subject() {
    return $this->services['translation']->t('Who\'s online');
  }

  /**
   * This seriously needs a new method name.  I know. I've discussed body()
   * with EclipseGc and neclimdul.  They're working in a slightly different direction
   * with plugin blocks, but for now this is sufficient.
   */
  public function run() {
      // $this->config is CMI stuff.  It is, whatever that ends up being.
      $interval = $this->request->server['REQUEST_TIME'] - $this->config->seconds_online;

      // By not using the hard-coded function here, we're injecting the
      // connection object, along with all other services!  That means we can
      // unit test this class with a completely fake database connection, if
      // we were so inclined.  It also means that we don't need to ensure
      // the function is defined before this class.  We can just let the
      // services object and the class loader sort it out for us.
      $authenticated_count = $this->services['database']->query("SELECT COUNT(DISTINCT s.uid) FROM {sessions} s WHERE s.timestamp >= :timestamp AND s.uid > 0", array(':timestamp' => $interval))->fetchField();

      // I'm leaving the $request object out of here (where it is in cats.module)
      // because this is how I want it to work eventually.  Actually once it's
      // methods we could make the formatPlural signature nicer too, but I
      // am not getting into that yet.
      $output = $this->services['translation']->formatPlural($authenticated_count, 'There is currently 1 user online.', 'There are currently @count users online.');

      // Once again, this is a CMI thing and replaces the variable_get() calls.
      $max_users = $this->config->max_list_count;
      if ($authenticated_count && $max_users) {
        $items = $this->services['database']->queryRange('SELECT u.uid, u.name, MAX(s.timestamp) AS max_timestamp FROM {users} u INNER JOIN {sessions} s ON u.uid = s.uid WHERE s.timestamp >= :interval AND s.uid > 0 GROUP BY u.uid, u.name ORDER BY max_timestamp DESC', 0, $max_users, array(':interval' => $interval))->fetchAll();

        // Everything I said above about the database injection?  It applies
        // here, too, for the theme.
        $output .= $this->services['theme']->theme('user_list', array('users' => $items));
      }

      return $output;
  }

}
