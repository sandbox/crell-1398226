<?php

namespace Drupal\Module\cats;

use Drupal\Core\ServiceRegistrationInterface;
use Drupal\Core\ServiceContainer;

/**
 * Use this class if you want to offer up a new subsystem for other modules to
 * leverage.  If you don't want to offer such a service to other modules, skip
 * this entire file.
 */
class ServiceRegistration implements ServiceRegistrationInterface {

  public function registerServices(ServiceContainer $services) {

    // There are many kittens. (Every call to $services['kitten'] returns a
    // new instance of Kitten).
    $services['kitten'] = function($container) {
      return new Kitten();
    };

    // But there is only one Rodney.  (The same instance of Rodney is always
    // returned.)
    $services['rodney'] = $services->share(function($container) {
      return new Rodney();
    });

    // More realistically, this provides access to the system responsible for
    // managing hairballs.
    $services['hairball'] = $services->share(function($container) {
      return new HairballManager();
    });

  }
}
