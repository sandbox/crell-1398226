<?php

namespace Drupal\Module\cats;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Drupal\Core\ServiceUserInterface;
use Drupal\Core\ServiceContainer;

use Exception;

/**
 * I think we could probably add a core base class that this extends to handle
 * the constructor and the interfaces.  That would make this even simpler.
 *
 * Use this class to provide a default, lazy-loaded value for request-derived
 * information.
 */
class ContextSubscriber implements EventSubscriberInterface, ServiceUserInterface {
  protected $logger;

  /**
   * The service container for this controller.
   *
   * @var ServiceContainer
   */
  protected $services;

  public function __construct(LoggerInterface $logger = null) {
    // inject eventually other dependencies too if using an object to load the node
    $this->logger = $logger;
  }

  public function setServiceContainer(ServiceContainer $services) {
    $this->services = $services;
  }

  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();

    $services = $this->services;
    $request->context['cat'] = $request->context->share(function($context) use ($services) {
      // Do something to figure out the right cat.
        return new Kitten();
      }
    );

    // Because this is not shared, it will be re-called every time. That is useful
    // for "second level" stuff, where it could change if the value it derives
    // from changes.
    $request->context['hairballs'] = function($context) use ($services) {
      return $services['hairball']->countHairballs($context['cat']);
    };
  }

  static public function getSubscribedEvents() {
    return array(
      KernelEvents::REQUEST => array('onKernelRequest', 25),
    );
  }
}
